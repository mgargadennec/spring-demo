package com.cardiweb.bo.group.service;

import java.util.List;

import com.cardiweb.bo.group.model.Group;
import com.cardiweb.bo.user.model.User;


public interface GroupService {

	public String createGroup(Group group);

	public Group getGroup(String id);

	public List<Group> getGroups();

	public void add(User user, Group group);

	public void remove(User user, Group group);
	
	public List<User> getUsers(String groupId);
	
	public List<Group> getGroups(String userId);
	
}
