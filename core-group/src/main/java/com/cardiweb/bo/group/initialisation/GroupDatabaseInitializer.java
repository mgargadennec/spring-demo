package com.cardiweb.bo.group.initialisation;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.util.StringUtils;

import com.cardiweb.bo.group.configuration.GroupConfigurationProperties;

public class GroupDatabaseInitializer {

	private static final Logger LOGGER = LoggerFactory.getLogger(GroupDatabaseInitializer.class);

	private GroupConfigurationProperties properties;

	private DataSource dataSource;

	private ResourceLoader resourceLoader;

	public GroupDatabaseInitializer(GroupConfigurationProperties properties,
			DataSource dataSource, ResourceLoader resourceLoader) {
		this.properties = properties;
		this.dataSource = dataSource;
		this.resourceLoader = resourceLoader;
	}

	@PostConstruct
	protected void initialize() {
		if (this.properties.getInitializer().isEnabled()) {
			LOGGER.info("Creating group database from schema "+properties.getSchema());

			ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
			String schemaLocation = this.properties.getSchema();
			for(Resource resource : getResources(schemaLocation)){
				populator.addScript(resource);	
			}
			populator.setContinueOnError(false);
			DatabasePopulatorUtils.execute(populator, this.dataSource);
		}
	}

	private List<Resource> getResources(String locations) {
		List<Resource> resources = new ArrayList<Resource>();
		for (String location : StringUtils.commaDelimitedListToStringArray(locations)) {
			Resource resource = resourceLoader.getResource(location);
			resources.add(resource);
		}
		return resources;
	}
}
