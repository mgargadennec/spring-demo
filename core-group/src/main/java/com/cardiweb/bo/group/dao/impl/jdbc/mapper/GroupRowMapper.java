package com.cardiweb.bo.group.dao.impl.jdbc.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cardiweb.bo.group.model.Group;

public class GroupRowMapper implements RowMapper<Group>{
	@Override
	public Group mapRow(ResultSet rs, int rowNum) throws SQLException {
		Group user = new Group();
		user.setId(rs.getString("id"));
		user.setNom(rs.getString("nom"));
		user.setCreation(rs.getTimestamp("creation_date"));
		user.setModification(rs.getTimestamp("modification_date"));
		return user;
	}
}
