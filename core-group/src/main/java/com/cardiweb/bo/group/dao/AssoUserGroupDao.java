package com.cardiweb.bo.group.dao;

import java.util.List;

import com.cardiweb.bo.group.model.Group;
import com.cardiweb.bo.user.model.User;

public interface AssoUserGroupDao {
	
	void add(String groupId, String userId);

	void remove(String groupId, String userId);

	List<Group> getGroups(String userId);

	List<User> getUsers(String groupId);
}
