package com.cardiweb.bo.group.dao;

import java.util.List;

import com.cardiweb.bo.group.model.Group;

public interface GroupDao {
	
	String create(Group group);

	Group retrieve(String id);
	
	void update(Group user);

	void delete(String id);
	
	List<Group> retrieve();

}
