package com.cardiweb.bo.group.service.impl;

import java.util.List;
import java.util.UUID;

import com.cardiweb.bo.group.dao.AssoUserGroupDao;
import com.cardiweb.bo.group.dao.GroupDao;
import com.cardiweb.bo.group.model.Group;
import com.cardiweb.bo.group.service.GroupService;
import com.cardiweb.bo.user.model.User;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

public class GroupServiceImpl implements GroupService {

	private GroupDao groupDao;
	private AssoUserGroupDao assoUserGroupDao;
	
	public GroupServiceImpl(GroupDao groupDao, AssoUserGroupDao assoUserGroupDao) {
		this.groupDao = groupDao;
		this.assoUserGroupDao=assoUserGroupDao;
	}

	@Override
	public String createGroup(Group group) {
		group.setId(UUID.randomUUID().toString());
		return groupDao.create(group);
	}
	
	@Override
	public Group getGroup(String id) {
		return groupDao.retrieve(id);
	}

	@Override
	public List<Group> getGroups() {
		return groupDao.retrieve();
	}


	@Override
	public void add(User user, Group group) {
		Preconditions.checkState(!Strings.isNullOrEmpty(user.getId())," User must be saved first.");
		Preconditions.checkState(!Strings.isNullOrEmpty(group.getId())," Group must be saved first.");
		
		assoUserGroupDao.add(group.getId(), user.getId());
	}

	@Override
	public void remove(User user, Group group) {
		Preconditions.checkState(!Strings.isNullOrEmpty(user.getId())," User must be saved first.");
		Preconditions.checkState(!Strings.isNullOrEmpty(group.getId())," Group must be saved first.");
		
		assoUserGroupDao.remove(group.getId(), user.getId());
	}
	
	@Override
	public List<User> getUsers(String groupId) {
		return assoUserGroupDao.getUsers(groupId);
	}

	@Override
	public List<Group> getGroups(String userId) {
		return assoUserGroupDao.getGroups(userId);
	}
}
