package com.cardiweb.bo.group;

import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ResourceLoader;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.cardiweb.bo.group.configuration.GroupConfigurationProperties;
import com.cardiweb.bo.group.dao.AssoUserGroupDao;
import com.cardiweb.bo.group.dao.GroupDao;
import com.cardiweb.bo.group.dao.impl.jdbc.AssoUserGroupDaoJdbc;
import com.cardiweb.bo.group.dao.impl.jdbc.GroupDaoJdbc;
import com.cardiweb.bo.group.dao.impl.jdbc.mapper.GroupRowMapper;
import com.cardiweb.bo.group.initialisation.GroupDatabaseInitializer;
import com.cardiweb.bo.group.service.GroupService;
import com.cardiweb.bo.group.service.impl.GroupServiceImpl;
import com.cardiweb.bo.group.service.impl.UserServiceImpl;
import com.cardiweb.bo.user.BoUserAutoConfiguration;
import com.cardiweb.bo.user.dao.UserDao;
import com.cardiweb.bo.user.dao.impl.jdbc.mapper.UserRowMapper;
import com.cardiweb.bo.user.initialisation.UserDatabaseInitializer;
import com.cardiweb.bo.user.service.UserService;

@Configuration
@AutoConfigureAfter(BoUserAutoConfiguration.class)
public class BoGroupAutoConfiguration {
	
	@Bean
	public GroupConfigurationProperties groupConfigurationProperties(){
		return new GroupConfigurationProperties();
	}
	
	@Bean
	public GroupRowMapper groupRowMapper(){
		return new GroupRowMapper();
	}

	@Bean 
	public GroupDao groupDao(NamedParameterJdbcTemplate jdbcTemplate,GroupRowMapper groupRowMapper){
		return new GroupDaoJdbc(jdbcTemplate,groupRowMapper);
	}

	@Bean 
	public AssoUserGroupDao assoUserGroupDao(NamedParameterJdbcTemplate jdbcTemplate, UserRowMapper userRowMapper){
		return new AssoUserGroupDaoJdbc(jdbcTemplate,groupRowMapper(),userRowMapper);
	}

	@Bean 
	public GroupService groupService(GroupDao groupDao,AssoUserGroupDao assoGroupUserDao){
		return new GroupServiceImpl(groupDao,assoGroupUserDao);
	}

	@Bean
	public UserService userService(UserDao userDao,AssoUserGroupDao assoGroupUserDao){
		return new UserServiceImpl(userDao,assoGroupUserDao);
	}

	@Bean
	@ConditionalOnBean(UserDatabaseInitializer.class)
	public GroupDatabaseInitializer groupDataBaseInitializer(GroupConfigurationProperties groupConfigurationProperties,DataSource ds, ResourceLoader resourceLoader){
		return new GroupDatabaseInitializer(groupConfigurationProperties, ds ,resourceLoader);
	}
	
}
