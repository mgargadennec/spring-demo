package com.cardiweb.bo.group.service.impl;

import java.util.List;

import com.cardiweb.bo.group.dao.AssoUserGroupDao;
import com.cardiweb.bo.group.model.Group;
import com.cardiweb.bo.user.dao.UserDao;

public class UserServiceImpl extends com.cardiweb.bo.user.service.impl.UserServiceImpl {

	private AssoUserGroupDao assoUserGroupDao;
	
	public UserServiceImpl(UserDao userDao, AssoUserGroupDao assoUserGroupDao) {
		super(userDao);
		this.assoUserGroupDao=assoUserGroupDao;
	}
	
	@Override
	public void delete(String id) {
		List<Group> groups = assoUserGroupDao.getGroups(id);
		for(Group group : groups){
			assoUserGroupDao.remove(group.getId(), id);
		}
		super.delete(id);
	}
}
