package com.cardiweb.bo.group.dao.impl.jdbc;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.cardiweb.bo.group.dao.GroupDao;
import com.cardiweb.bo.group.dao.impl.jdbc.mapper.GroupRowMapper;
import com.cardiweb.bo.group.model.Group;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

public class GroupDaoJdbc implements GroupDao{

	private NamedParameterJdbcTemplate jdbcTemplate;
	private GroupRowMapper groupRowMapper;

	public GroupDaoJdbc(NamedParameterJdbcTemplate jdbcTemplate,GroupRowMapper groupRowMapper) {
		this.jdbcTemplate=jdbcTemplate;
		this.groupRowMapper=groupRowMapper;
	}

	@Override
	public String create(Group group) {
		Preconditions.checkState(!Strings.isNullOrEmpty(group.getId()),"The id must be null or empty");
		
		Timestamp currentTs = new Timestamp(System.currentTimeMillis());
		group.setCreation(currentTs);
		group.setModification(currentTs);

		jdbcTemplate.update("insert into bo_group values(:id,:nom,:creationDate,:modificationDate)", toMap(group));
		return group.getId();
	}

	@Override
	public void update(Group group) {
		group.setModification(new Timestamp(System.currentTimeMillis()));
		jdbcTemplate.update("update bo_group set nom=:nom, modification_date=:modificationDate where id=:id", toMap(group));
	}

	@Override
	public Group retrieve(String id) {
		return jdbcTemplate.queryForObject("select * from bo_group where id=:id",new MapSqlParameterSource("id", id),groupRowMapper);
	}
	
	@Override
	public List<Group> retrieve() {
		return jdbcTemplate.query("select * from bo_group",new HashMap<String,Object>(),groupRowMapper);
	}

	@Override
	public void delete(String id) {
		jdbcTemplate.update("delete from bo_group where id=:id",new MapSqlParameterSource("id", id));
	}


	private Map<String,Object> toMap(Group group) {
		Map<String,Object> mappedUser = new HashMap<String, Object>();
		mappedUser.put("id", group.getId());
		mappedUser.put("nom", group.getNom());
		mappedUser.put("creationDate", group.getCreation());
		mappedUser.put("modificationDate", group.getModification());
		return mappedUser;
	}
}
