package com.cardiweb.bo.group.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("cardiboot.user")
public class GroupConfigurationProperties {
	
	private static final String DEFAULT_SCHEMA_LOCATION = "classpath:sql/group.sql,classpath:sql/asso_user_group.sql";
	
	private String schema = DEFAULT_SCHEMA_LOCATION;
	private final Initializer initializer = new Initializer();

	public static class Initializer {
		private boolean enabled = true;

		public boolean isEnabled() {
			return this.enabled;
		}

		public void setEnabled(boolean enabled) {
			this.enabled = enabled;
		}
	}

	public String getSchema() {
		return schema;
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public Initializer getInitializer() {
		return initializer;
	}
}
