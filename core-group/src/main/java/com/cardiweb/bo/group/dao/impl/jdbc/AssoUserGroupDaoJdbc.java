package com.cardiweb.bo.group.dao.impl.jdbc;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.cardiweb.bo.group.dao.AssoUserGroupDao;
import com.cardiweb.bo.group.dao.impl.jdbc.mapper.GroupRowMapper;
import com.cardiweb.bo.group.model.Group;
import com.cardiweb.bo.user.dao.impl.jdbc.mapper.UserRowMapper;
import com.cardiweb.bo.user.model.User;

public class AssoUserGroupDaoJdbc implements AssoUserGroupDao{

	private NamedParameterJdbcTemplate jdbcTemplate;
	private GroupRowMapper groupRowMapper;
	private UserRowMapper userRowMapper;

	public AssoUserGroupDaoJdbc(NamedParameterJdbcTemplate jdbcTemplate,GroupRowMapper groupRowMapper, UserRowMapper userRowMapper) {
		this.jdbcTemplate=jdbcTemplate;
		this.groupRowMapper=groupRowMapper;
		this.userRowMapper=userRowMapper;
	}

	@Override
	public void add(String groupId, String userId) {
		jdbcTemplate.update("insert into bo_asso_user_group values(:groupId,:userId,:creationDate,:modificationDate)", toMap(groupId,userId,new Timestamp(System.currentTimeMillis())));
	}

	@Override
	public void remove(String groupId, String userId) {
		jdbcTemplate.update("delete from bo_asso_user_group where group_id=:groupId and user_id=:userId", toMap(groupId,userId,new Timestamp(System.currentTimeMillis())));
	}

	@Override
	public List<Group> getGroups(String userId) {
		return jdbcTemplate.query("select g.* from bo_asso_user_group a inner join bo_group g on a.group_id=g.id where a.user_id=:userId",new MapSqlParameterSource("userId", userId),groupRowMapper);
	}

	@Override
	public List<User> getUsers(String groupId) {
		return jdbcTemplate.query("select u.* from bo_asso_user_group a inner join bo_user u on a.group_id=u.id where a.group_id=:groupId",new MapSqlParameterSource("groupId", groupId),userRowMapper);
	}

	private Map<String,Object> toMap(String groupId,String userId,Timestamp currentTs) {
		Map<String,Object> mappedUser = new HashMap<String, Object>();
		mappedUser.put("groupId", groupId);
		mappedUser.put("userId", userId);
		mappedUser.put("creationDate", currentTs);
		mappedUser.put("modificationDate", currentTs);
		return mappedUser;
	}
}
