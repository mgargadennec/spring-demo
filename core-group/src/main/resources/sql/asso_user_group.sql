create table if not exists bo_asso_user_group (
  group_id varchar(40),
  user_id varchar(40),
  creation_date timestamp not null,
  modification_date timestamp not null,
  PRIMARY KEY (group_id, user_id),
  CONSTRAINT fk_group FOREIGN KEY (group_id) REFERENCES bo_group(id),
  CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES bo_user(id)
);