create table if not exists bo_group (
  id varchar(40) primary key,
  nom varchar(50),
  creation_date timestamp not null,
  modification_date timestamp not null
);