create table if not exists bo_user (
  id varchar(40) primary key,
  nom varchar(30),
  prenom varchar(30),
  login varchar(30),
  creation_date timestamp not null,
  modification_date timestamp not null,
);