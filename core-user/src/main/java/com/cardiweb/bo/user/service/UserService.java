package com.cardiweb.bo.user.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.cardiweb.bo.user.model.User;


public interface UserService {

	@Transactional
	public String createUser(User user);

	@Transactional
	public void delete(String id);

	public User getUser(String id);
	
	public List<User> getUsers();

	
}
