package com.cardiweb.bo.user.initialisation;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ResourceLoader;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;

import com.cardiweb.bo.user.configuration.UserConfigurationProperties;

public class UserDatabaseInitializer {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserDatabaseInitializer.class);
	
	private UserConfigurationProperties properties;
	
	private DataSource dataSource;
	
	private ResourceLoader resourceLoader;

	public UserDatabaseInitializer(UserConfigurationProperties properties,
			DataSource dataSource, ResourceLoader resourceLoader) {
		this.properties = properties;
		this.dataSource = dataSource;
		this.resourceLoader = resourceLoader;
	}

	@PostConstruct
	protected void initialize() {
		if (this.properties.getInitializer().isEnabled()) {
			LOGGER.info("Creating User database from schema "+properties.getSchema());
			ResourceDatabasePopulator populator = new ResourceDatabasePopulator();
			String schemaLocation = this.properties.getSchema();
			populator.addScript(this.resourceLoader.getResource(schemaLocation));
			populator.setContinueOnError(false);
			DatabasePopulatorUtils.execute(populator, this.dataSource);
		}
	}
}
