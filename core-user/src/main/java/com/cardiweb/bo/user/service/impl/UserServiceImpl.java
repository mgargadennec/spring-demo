package com.cardiweb.bo.user.service.impl;

import java.util.List;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cardiweb.bo.user.dao.UserDao;
import com.cardiweb.bo.user.model.User;
import com.cardiweb.bo.user.service.UserService;

public class UserServiceImpl implements UserService {

	private final static Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
	
	private UserDao userDao;
	
	public UserServiceImpl(UserDao userDao) {
		this.userDao = userDao;
	}
	
	@Override
	public User getUser(String id) {
		long start = System.currentTimeMillis();
		User user = userDao.retrieve(id);
		LOGGER.info("User retrieved from DB in "+(System.currentTimeMillis()-start)+" ms.");
		return user;
	}

	@Override
	public List<User> getUsers() {
		return userDao.retrieve();
	}

	@Override
	public String createUser(User user) {
		user.setId(UUID.randomUUID().toString());
		return userDao.create(user);
	}

	@Override
	public void delete(String id) {
		userDao.delete(id);
	}

}
