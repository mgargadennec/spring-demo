package com.cardiweb.bo.user.dao.impl.jdbc.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cardiweb.bo.user.model.User;

public class UserRowMapper implements RowMapper<User> {
	@Override
	public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		User user = new User();
		user.setId(rs.getString("id"));
		user.setPrenom(rs.getString("prenom"));
		user.setNom(rs.getString("nom"));
		user.setLogin(rs.getString("login"));
		user.setCreation(rs.getTimestamp("creation_date"));
		user.setModification(rs.getTimestamp("modification_date"));
		return user;
	}
}
