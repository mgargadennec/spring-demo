package com.cardiweb.bo.user.dao.impl.jdbc;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.cardiweb.bo.user.dao.UserDao;
import com.cardiweb.bo.user.dao.impl.jdbc.mapper.UserRowMapper;
import com.cardiweb.bo.user.model.User;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;

public class UserDaoJdbc implements UserDao{
	
	private final static Logger LOGGER = LoggerFactory.getLogger(UserDaoJdbc.class);

	private NamedParameterJdbcTemplate jdbcTemplate;
	private UserRowMapper userRowMapper;

	public UserDaoJdbc(NamedParameterJdbcTemplate jdbcTemplate,UserRowMapper userRowMapper) {
		this.jdbcTemplate = jdbcTemplate;
		this.userRowMapper = userRowMapper;
	}

	@Override
	public String create(User user) {
		Preconditions.checkState(!Strings.isNullOrEmpty(user.getId()),"The id must not be null or empty");
		
		Timestamp currentTs = new Timestamp(System.currentTimeMillis());
		user.setCreation(currentTs);
		user.setModification(currentTs);

		jdbcTemplate.update("insert into bo_user values(:id,:prenom,:nom,:login,:creationDate,:modificationDate)", toMap(user));
		return user.getId();
	}

	@Override
	@CachePut(value="users", key="#user.id")
	public User update(User user) {
		user.setModification(new Timestamp(System.currentTimeMillis()));
		jdbcTemplate.update("update bo_user set prenom=:prenom, nom=:nom , modification_date=:modificationDate where id=:id", toMap(user));
		return user;
	}

	@Override
	public User retrieve(String id) {
		simulateSlowService();
		return jdbcTemplate.queryForObject("select * from bo_user where id=:id",new MapSqlParameterSource("id", id),userRowMapper);
	}
	
	@Override
	public List<User> retrieve() {
		return jdbcTemplate.query("select * from bo_user",new HashMap<String,Object>(),userRowMapper);
	}

	@Override
	@CacheEvict(value="users", key="#id")
	public void delete(String id) {
		jdbcTemplate.update("delete from bo_user where id=:id",new MapSqlParameterSource("id", id));
	}

	private Map<String,Object> toMap(User user) {
		Map<String,Object> mappedUser = new HashMap<String, Object>();
		mappedUser.put("id", user.getId());
		mappedUser.put("nom", user.getNom());
		mappedUser.put("prenom", user.getPrenom());
		mappedUser.put("login", user.getLogin());
		mappedUser.put("creationDate", user.getCreation());
		mappedUser.put("modificationDate", user.getModification());
		return mappedUser;
	}
	
	
	// Don't do this at home
    private void simulateSlowService() {
        try {
            long time = (long) (5000L);
            Thread.sleep(time);
        } catch (InterruptedException e) {
            throw new IllegalStateException(e);
        }
    }
}
