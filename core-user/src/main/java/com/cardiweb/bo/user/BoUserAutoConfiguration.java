package com.cardiweb.bo.user;

import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ResourceLoader;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.cardiweb.bo.user.configuration.UserConfigurationProperties;
import com.cardiweb.bo.user.dao.UserDao;
import com.cardiweb.bo.user.dao.impl.jdbc.UserDaoJdbc;
import com.cardiweb.bo.user.dao.impl.jdbc.mapper.UserRowMapper;
import com.cardiweb.bo.user.initialisation.UserDatabaseInitializer;
import com.cardiweb.bo.user.service.UserService;
import com.cardiweb.bo.user.service.impl.UserServiceImpl;

@Configuration
@AutoConfigureAfter(DataSourceAutoConfiguration.class)
public class BoUserAutoConfiguration {

	@Bean
	public UserConfigurationProperties userConfigurationProperties(){
		return new UserConfigurationProperties();
	}

	@Bean
	public UserRowMapper userRowMapper(){
		return new UserRowMapper();
	}

	@Bean 
	@ConditionalOnMissingBean(UserDao.class)
	public UserDao userDao(NamedParameterJdbcTemplate jdbcTemplate,UserRowMapper userRowMapper){
		return new UserDaoJdbc(jdbcTemplate, userRowMapper);
	}

	@Bean 
	@ConditionalOnMissingBean(UserService.class)
	public UserService userService(UserDao userDao){
		return new UserServiceImpl(userDao);
	}

	@Bean
	@ConditionalOnBean(DataSource.class)
	public UserDatabaseInitializer dataBaseInitializer(UserConfigurationProperties userConfigurationProperties, DataSource ds, ResourceLoader resourceLoader){
		return new UserDatabaseInitializer(userConfigurationProperties,ds,resourceLoader);
	}

}
