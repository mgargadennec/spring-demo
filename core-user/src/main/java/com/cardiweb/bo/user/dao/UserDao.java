package com.cardiweb.bo.user.dao;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;

import com.cardiweb.bo.user.model.User;

public interface UserDao {
	
	String create(User user);

	@Cacheable(value="users",key="#id")
	User retrieve(String id);
	
	User update(User user);

	void delete(String id);
	
	List<User> retrieve();

}
