package demo.custom;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.cardiweb.bo.user.dao.UserDao;
import com.cardiweb.bo.user.model.User;
import com.google.common.collect.Lists;

public class CustomUserDaoImpl implements UserDao {

	private Map<String,User> users = new HashMap<String, User>();

	@Override
	public String create(User user) {
		Timestamp currentTs = new Timestamp(System.currentTimeMillis());

		user.setCreation(currentTs);
		user.setModification(currentTs);
		
		users.put(user.getId(), user);

		return user.getId();
	}

	@Override
	public User retrieve(String id) {
		return users.get(id);
	}

	@Override
	public List<User> retrieve() {
		return Lists.newArrayList(users.values());
	}

	@Override
	public User update(User user) {
		user.setModification(new Timestamp(System.currentTimeMillis()));
		users.put(user.getId(), user);
		return user;
	}

	@Override
	public void delete(String id) {
		users.remove(id);
	}

}
