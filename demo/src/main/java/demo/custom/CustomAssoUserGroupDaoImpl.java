package demo.custom;

import java.sql.Timestamp;
import java.util.List;

import com.cardiweb.bo.group.dao.AssoUserGroupDao;
import com.cardiweb.bo.group.dao.GroupDao;
import com.cardiweb.bo.group.model.Group;
import com.cardiweb.bo.user.dao.UserDao;
import com.cardiweb.bo.user.model.User;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

public class CustomAssoUserGroupDaoImpl implements AssoUserGroupDao {

	private List<Asso> assos = Lists.newArrayList();
	
	private UserDao userDao;
	private GroupDao groupDao;
	
	public CustomAssoUserGroupDaoImpl(UserDao userDao, GroupDao groupDao) {
		this.userDao = userDao;
		this.groupDao = groupDao;
	}

	@Override
	public void add(String groupId, String userId) {
		assos.add(new Asso(groupId,userId));
	}

	@Override
	public void remove(final String groupId,final String userId) {
		Iterables.removeIf(assos, new Predicate<Asso>() {
			@Override
			public boolean apply(Asso input) {
				return input.groupId.equals(groupId) && input.userId.equals(userId);
			}
		});
	}

	@Override
	public List<Group> getGroups(final String userId) {
		Predicate<Asso> userPredicate = new Predicate<Asso>() {
			@Override
			public boolean apply(Asso input) {
				return input.userId.equals(userId);
			}
		};
		
		if(!Iterables.any(assos, userPredicate)){
			throw new RuntimeException("User "+userId+" not found !");
		}
		
		return FluentIterable.from(assos).filter(userPredicate).transform(new Function<Asso, Group>(){
			@Override
			public Group apply(Asso input) {
				return groupDao.retrieve(input.groupId);
			}
			
		}).toList();
	}

	@Override
	public List<User> getUsers(final String groupId) {
		Predicate<Asso> groupPredicate = new Predicate<Asso>() {
			@Override
			public boolean apply(Asso input) {
				return input.groupId.equals(groupId);
			}
		};
		
		if(!Iterables.any(assos, groupPredicate)){
			throw new RuntimeException("Group "+groupId+" not found !");
		}
		
		return FluentIterable.from(assos).filter(groupPredicate).transform(new Function<Asso, User>(){
			@Override
			public User apply(Asso input) {
				return userDao.retrieve(input.userId);
			}
		}).toList();
	}
	
	private class Asso{

		String groupId;
		String userId;
		Timestamp dateCreation;
		Timestamp dateModification;
		
		private Asso(String groupId, String userId) {
			this.groupId=groupId;
			this.userId=userId;
			this.dateCreation=new Timestamp(System.currentTimeMillis());
			this.dateModification=new Timestamp(System.currentTimeMillis());
		}
		
	}

}
