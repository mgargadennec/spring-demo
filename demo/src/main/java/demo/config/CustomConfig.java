package demo.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CustomConfig {

	private static final Logger LOGGER = LoggerFactory.getLogger(CustomConfig.class);
	
 
//	@Bean
//	public UserDao customUserDao(){
//		return new CustomUserDaoImpl();
//	}
//
//	@Bean
//	public AssoUserGroupDao customAssoUserGroupDao(UserDao userDao, GroupDao groupDao){
//		return new CustomAssoUserGroupDaoImpl(userDao, groupDao);
//	}
}
