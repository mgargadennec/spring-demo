package demo.config;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import com.cardiweb.bo.user.model.User;
import com.cardiweb.bo.user.service.UserService;

@Configuration
@EnableScheduling
public class ScheduledTaskConfiguration {
	private static final Logger LOGGER = LoggerFactory.getLogger(ScheduledTaskConfiguration.class);

	@Autowired UserService userService;
	
	@Scheduled(fixedDelay=60000,initialDelay=10000)
	public void task(){
		LOGGER.info("Oh non, un utilisateur se prépare à partir !");
		List<User> users = userService.getUsers();
		if(!users.isEmpty()){
			Collections.shuffle(users);
			LOGGER.info("{} s'en va !",users.get(0).getId());
			userService.delete(users.get(0).getId());
			LOGGER.info("Et voila, je vous parie que le prochain partira dans 1 minute... (nous ne sommes plus que {} !)",users.size()-1);
		}else{
			LOGGER.info("Tous les utilisateurs sont partis :(");
		}
	}

}
