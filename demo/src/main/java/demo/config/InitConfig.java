package demo.config;

import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.Transactional;

import com.cardiweb.bo.group.model.Group;
import com.cardiweb.bo.group.service.GroupService;
import com.cardiweb.bo.user.model.User;
import com.cardiweb.bo.user.service.UserService;

@Configuration
public class InitConfig {

	private static final Logger LOGGER = LoggerFactory.getLogger(InitConfig.class);

	@Bean
	public CommandLineRunner clr(final UserService userService, final GroupService groupService){
		LOGGER.info("Injecting data into db for initialisation");
		return new CommandLineRunner() {
			@Override
			@Transactional
			public void run(String... args) throws Exception {

				Random random = new Random();
				int userCount = random.nextInt(300);

				for(int i=0;i<userCount;i++){
					User user = new User();
					user.setPrenom("prenom"+i);
					user.setNom("nom"+i);
					user.setLogin("login"+i);
					userService.createUser(user);	
				}

				List<User> users =userService.getUsers();
				LOGGER.info("Utilisateurs créés : "+users.size());

				int groupCount = random.nextInt(25);
				for(int i=0;i<groupCount;i++){
					Group group = new Group();
					group.setNom("groupe"+i);

					groupService.createGroup(group);
				}
				List<Group> groups = groupService.getGroups();
				LOGGER.info("Groupes créés : "+groups.size());

				int assoCounter = 0;
				for(Group group : groups){
					//On mélange les users à chaque fois
					Collections.shuffle(users);

					//On en prend un nombre aléatoire
					List<User> pickedUsers = users.subList(0, random.nextInt(users.size()));

					for(User user : pickedUsers){
						groupService.add(user,group);
						assoCounter++;
					}
				}
				LOGGER.info("Association créées : "+assoCounter);
			}
		};
	}
}
