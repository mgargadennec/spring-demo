package demo.config;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.cache.guava.GuavaCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.cache.CacheBuilder;

@Configuration
@EnableCaching
public class CacheConfig {

	private static final Logger LOGGER = LoggerFactory.getLogger(CacheConfig.class);
//
//	@Bean
//	public CacheManager cacheManagerConcurrentMap(){
//    	return new ConcurrentMapCacheManager();
//	}
	
    @Bean
    public CacheManager cacheManagerGuava() {
    	GuavaCacheManager cacheManager = new GuavaCacheManager();
    	cacheManager.setCacheBuilder(CacheBuilder.newBuilder()
    		       .maximumSize(1000)
    		       .expireAfterWrite(10, TimeUnit.MINUTES));
    	return cacheManager;
    }
}
