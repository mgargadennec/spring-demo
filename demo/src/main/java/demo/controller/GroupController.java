package demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cardiweb.bo.group.model.Group;
import com.cardiweb.bo.group.service.GroupService;
import com.cardiweb.bo.user.model.User;

@Controller
@RequestMapping("/groups")
public class GroupController {

	@Autowired
	GroupService groupService;

	@RequestMapping
	public @ResponseBody List<Group> groups(){
		return groupService.getGroups();
	}

	@RequestMapping("/{groupId}")
	public @ResponseBody Group group(@PathVariable String groupId){
		return groupService.getGroup(groupId);
	}

	@RequestMapping("/{groupId}/users")
	public @ResponseBody List<User> usersForGroup(@PathVariable String groupId){
		return groupService.getUsers(groupId);
	}
}
