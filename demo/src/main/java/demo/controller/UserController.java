package demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cardiweb.bo.group.model.Group;
import com.cardiweb.bo.group.service.GroupService;
import com.cardiweb.bo.user.model.User;
import com.cardiweb.bo.user.service.UserService;

@Controller
@RequestMapping("/users")
public class UserController {

	@Autowired UserService userService;
	@Autowired GroupService groupService;

	@RequestMapping
	public @ResponseBody List<User> users(){
		return userService.getUsers();
	}

	@RequestMapping("/{userId}")
	public @ResponseBody User user(@PathVariable String userId){
		return userService.getUser(userId);
	}

	@RequestMapping("/{userId}/groups")
	public @ResponseBody List<Group> groupsForUser(@PathVariable String userId){
		return groupService.getGroups(userId);
	}

	@RequestMapping("/{userId}/delete")
	public void deleteUser(@PathVariable String userId){
		userService.delete(userId);
	}
}
