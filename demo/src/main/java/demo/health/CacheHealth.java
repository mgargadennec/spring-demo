package demo.health;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.Health.Builder;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Component;

import com.google.common.collect.Maps;

@Component
public class CacheHealth implements HealthIndicator {

	@Autowired
	CacheManager cacheManager;

	@Override
	public Health health() {
		if(cacheManager!=null){
			Builder builder = Health.up();
			Map<String, CacheInfos> caches = Maps.newHashMap();
			for(String cacheName : cacheManager.getCacheNames()){
				Cache cache = cacheManager.getCache(cacheName);
				Object nativeCache = cache.getNativeCache();
				Integer counter = nativeCache instanceof Map ?  ((Map)nativeCache).size(): null;
				caches.put(cacheName, new CacheInfos(cacheManager.getCache(cacheName).getClass().toString(), counter));
			}
			builder.withDetail("caches", caches);
			return builder.build();
		}
		return Health.down().build();
	}
	
	public class CacheInfos{
		private String type;
		private Integer count;
		
		public CacheInfos(String type, Integer count){
			this.type=type;
			this.count=count;
		}

		public String getType() {
			return type;
		}

		public Integer getCount() {
			return count;
		}
	}
}